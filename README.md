# Accelerate reading notes

## Measuring Software Delivery Performance

Objective: Balance between delivery and quality of work

Key Measures:
 - **Delivery Lead Time***: The time it takes for code to be commited until running properly in production.
 - **Deployment Frequency**: How many times per day software is delivered.
 - **Time to Restore Service (MTTR)**: How long it takes to restore the service in case of an incident.
 - **Change Fail Rate**: Percentage of failed changes in production.

*Design vs Delivery (Product Design != Product Delivery)

| Product Design and Development                              | Product Delivery (Build, Test, Deploy)        |
|-------------------------------------------------------------|-----------------------------------------------|
| User centered design                                        | Fast flow from development to production      |
| Design and implementation can be slow and never done before | Integration, test and deployment must be fast |
| Estimates are highly uncertain                              | Predictable cycle times                       |
| Outcomes are highly variable                                | Outcomes should have low variability          |

Some initial data to compare:

| **2017**                  | **High Performers**  |
|---------------------------|----------------------|
| **Deployment Frequency**  | multiple times a day |
| **Lead Time for Changes** | less than one hour   |
| **MTTR**                  | less than one hour   |
| **Change Failure Rate**   | 0-15%                |

## Software Delivery Performance impacts on commercial performance and noncommercial performance

- Delivering small batches allows faster feedback loops and hypothesis driven development
- Is preferable to build software that is strategic to the business in-house and outsource tools that are not (Wardling mapping method)


## Driving change

- Culture can be measured

### Westrum's typology of organizational cultures

- **Pathological (power-oriented)**: Political and hierarchical culture, driven by fear
- **Bureaucratic (rule-oriented)**: Siloed organizations thar work by the rules of the in-group
- **Generative (performance-oriented)**: Organizations that focus on the global mission and act locally

**Definition of good information**:
1. It provides answers the receiver needs.
2. It is timely.
3. It is provided in a way that it can be effectivelly used by the receiver.

**For a good organization culture, aim for**:
- High cooperation
- Blameless culture
- Shared Risks
- Cross-domain teams
- Inovation

**A good information flow has great impact on organizational performance**
**Continuous delivery and Lean Management impacts Organizational Culture**

## Continuous Delivery

1. **Build quality in**
2. **Work in small batches**
3. **Computers perform repetitive tasks; people solve problems**
4. **Relentlessly persue continuous improvement**
5. **Everyone is responsible**

Continuous Delivery drivers:
- [ ] Version Control
- [ ] Deployment Automation
- [ ] Continuous Integration
- [ ] Trunk-Based Development
- [ ] Test Automation
- [ ] Test Data Management
- [ ] Shift Left on security
- [ ] Loosely couple architecture
- [ ] Empowered Teams
- [ ] Monitoring
- [ ] Proactive Notification

**Continuous Delivery leads to Less Deployment Pain and Less Burnout**

**Continuous Delivery correlates with less Unplanned Work**

## The Rugged Movement

Bring Infosec into DevOps

## Lean Management Practices

1. **Limit WIP to increase throughput**
2. **Visual Management: Display Quality and Performance Metrics easily**
3. **Feedback from Production: Use data collected from metrics to drive decision making**
4. **Lightweight Change Management Process: Code reviews and QA**

**External review doesn't lead to better quality and decreases delivery performance**

## Lean Product Development Practices

- Work in small batches
- Make flow of work visible
- Gather and implement customer feedback
- Team Experimentation

## Problems that lead to Burnout

- Work Overload
- Lack of Control over decisions
- Insuffiecient rewards
- Unsupportive Work Environment
- Absence of fairness on decision making processes
- Individual's values conflicts with Organizational values

## Employee Loyalty

- **Promoter**: Likes the organization, is highly engaged and would recommend working there.
- **Passive**: Is satisfied but less enthusiastic, can defect if things get worse
- **Detractors**: Expensive to accquire and retain, can hurt the business by negative review of the organization

## Characteristics of a Transformational Leader

- **Vision**: Has a clear understanding of where the organization is going
- **Inspirational communication**
- **Intellectual stimulation**: Challenges followers to think outside the box
- **Supportive Leadership**: Cares about his follower's needs and feelings
- **Personal Recognition**: Praises and acknowledges achievments and improvements of work

## DevOps facilitation tasks

- [ ] Create opportunities for learning and improving and make them accessible to everyone (like books)
- [ ] Establish dedicated training budget and make sure people know about it
- [ ] Encourage staff to attend technical conferences
- [ ] Set up internal hack days
- [ ] Organize "yak days", days to attack technical debt
- [ ] Hold regular mini-conferences (i.e. DevOpsDays format, prepared talks + open spaces)
- [ ] Give teams some time to experiment on new tools and technologies

## 24 Capabilities to drive improvement

1. **Use version control for all production artifacts**
2. **Automate your deployment process**
3. **Implement Continuous Integration**
4. **Use trunk-based development**
5. **Implement test automation**
6. **Support test data management**
7. **Shift left on security**
8. **Implement Continuous Delivery**
9. **Use loosely couple architecture**
10. **Architect for empowered teams**
11. **Gather and Implement customer feedback**
12. **Make the flow of work visible through the value stream**
13. **Work in small batches**
14. **Foster and enable team experimentation**
15. **Have a lightweight change approval processes**
16. **Monitor across application and infrastructure to inform business decisions**
17. **Check system health proactively**
18. **Improve processes and manage work with WIP limits**
19. **Visualize work to monitor quality and communicate throughout the team**
20. **Support a Generative Culture**
21. **Encourage and support learning**
22. **Support and facilitate collaboration among teams**
23. **Provide resources that make work meaningful**
24. **Support or embody transformational leadership**

